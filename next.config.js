const withPlugins = require('next-compose-plugins');
const sass = require('@zeit/next-sass');
const images = require('next-optimized-images')
const styleLintPlugin = require('stylelint-webpack-plugin')
const path = require('path')

const processStyleLint = (config, failOnError = false, emitErrors = false) => {
    config.plugins.push(
      new styleLintPlugin({
        configFile: path.resolve(__dirname, './.stylelintrc'),
        context: 'src',
        failOnError: true,
        emitErrors: emitErrors,
        syntax: 'scss',
        quiet: false
      })
    )
}

module.exports = withPlugins([
  [sass, {
    cssModules: true,
    cssLoaderOptions: {
      localIdentName: '[path]___[local]___[hash:base64:5]',
    }
   } 
  ],
  [images, {
    optimizeImages: false
  }]
])
