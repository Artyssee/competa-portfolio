module.exports = {
	collectCoverageFrom: [
	  '<rootDir>/src/**/*.{js,jsx}'
	],
	coveragePathIgnorePatterns: [
	  '<rootDir>/node_modules/',
	  '<rootDir>/src/build/',
	  '<rootDir>/src/static/'
	],
	'moduleFileExtensions': [
	  'web.js',
	  'js',
	  'json',
	  'web.jsx',
	  'jsx',
	  'node'
	],
	moduleNameMapper: {
	  '\\.(jpg|svg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/fileMock.js',
	  '\\.(css|less|scss|styl)$': 'identity-obj-proxy'
	},
	setupFiles: ['<rootDir>/jest.setup.js'],
	testMatch: [
	  '<rootDir>/src/**/__tests__/**/*.js?(x)',
	  '<rootDir>/src/**/?(*.)(spec|test).js?(x)'
	],
	testPathIgnorePatterns: ['<rootDir>/src/.next/', '<rootDir>/src/build/', '<rootDir>/node_modules/', '<rootDir>/export/'],
	testURL: 'http://localhost',
	snapshotSerializers: ['enzyme-to-json/serializer']
  }
  